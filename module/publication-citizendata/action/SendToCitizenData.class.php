<?php 

/**
 * Send a document to Citizen data (file on a OpenData server and index in a Solr server).
 *
 *
 * @license    http://cecill.info/licences/Licence_CeCILL_V2-fr.html  Cecill V2
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class SendToCitizenData extends ActionExecutor {

	public function go() {
		$indexeur = $this->getConnecteur('GED');
		$citizenDataForm = $this->getDonneesFormulaire();

		$description 	= $citizenDataForm->get('Objet');
		$date 			= $citizenDataForm->get('date_document');
		$documentTypeId = $citizenDataForm->get('type');
		$documentType	= $citizenDataForm->getFieldData('type')->getValue()[0];
		$documentName 	= urldecode($citizenDataForm->getFileName('document'));
		$documentPath 	= $citizenDataForm->getFilePath('document');

		$destinationDirectory 	= NULL;
		switch ($documentTypeId) {
			case 1:
				$destinationDirectory = '0_Actes_administratifs';
				break;
			case 2:
				$destinationDirectory = '1_Commande_publique';
				break;
			case 3:
				$destinationDirectory = '2_Urbanisme';
				break;
			case 4:
				$destinationDirectory = '3_Domaine_et_patrimoine';
				break;
			case 5:
				$destinationDirectory = '4_Finances_locales';
				break;
			case 6:
				$destinationDirectory = '5_Autres_domaines_de_competences';
				break;
			default:
				$destinationDirectory = '5_Autres_domaines_de_competences';
		}

		$files = array();
		$newFile = new DocumentFile(NormalizeString::normalize($documentName), $documentPath);
		array_push($files, $newFile);

		$document = new CitizenDocument($description, $destinationDirectory, $files);
		$document->setOrigin('publication_pastell');
		$document->setType($documentType);
		$document->setDate(DateTime::createFromFormat('Y-m-d', $date));

		// Retrieve SIREN with entity identifier
		$siren =  $this->objectInstancier->EntiteSQL->getSiren($this->id_e);
		$document->setOrganization($indexeur->sireneConsumer->getOrganizationInformations($siren));
		
		if ($indexeur->indexDocument($document, false, true)) {
            $this->addActionOK('Document envoyé vers données citoyennes');
            $this->setLastMessage('Le document a bien été déposé dans les données citoyennes');
            return true;
		} else {
            $this->setLastMessage('L\'envoi vers les données citoyennes a échoué : '.$indexeur->getLastError());
            return false;
		}
	}
}