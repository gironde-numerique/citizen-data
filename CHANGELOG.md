# 0.1
- Initialisation flux et connecteur citizendata

# 0.2
- Implémentation du transfert de fichiers en SFTP en complément du protocole WebDAV

# 0.3
- Prise en charge des serveurs mutualisés (plusieurs instances sur un même serveur).

# 0.4
- Vérification des droits de création/suppression du répertoire de cache et d'exécution du module Java de génération des PDF des documents budgétaires.