# Connecteur Pastell "Données citoyennes"

## Description
Connecteur pour Pastell > 2.0.12 permettant l'indexation de documents dans un serveur Solr avec dépôt du fichier sur un serveur de fichiers.
La consultation des fichiers est permise par le biais de l'interface de recherche des données citoyennes (projet DataSearchEngine) et la récupération des données au format OpenData peut être effectuée par le biais de l'API développée par le syndicat mixte Gironde Numérique.
L'indexation s'effectue avec les données de la collectivité récupérées par le biais de son numéro SIREN auprès du WebService Sirene V3 proposé par l'Insee.

- Le dépôt des documents peut s'effectuer sur un dépôt propre à la collectivité hébergé dans un sous-domaine du type :
data.marie-xxx.fr
- Ou peut se faire sur un hébergement externalisé à son domaine auquel cas le sous-domaine doit commencer par data :
data-mairie-xxx.girondenumerique.fr
- Possibilité d'employer plusieurs instances de serveurs de fichiers sur un même serveur mutualisé en spécifiant le nom du VHost.

## Pré-requis
- Serveur Apache avec PHP > 5.5
- Extension Apache Solr
- Pastell > 3.0
- Compte utilisateur Insee Sirene V3
- Serveur Solr
- En cas de transfert SFTP des fichiers, déposer les fichiers de clés dans le répertoire /.ssh du serveur
- Vérifier les droits d'écriture du dossier connecteur/cache (écriture/lecture/suppression)
- Vérifier les droits d'exécution du fichier du dossier connecteur/module/print-comp.jar

## Installation
- Pour obtenir les dépendances du projet, lancer dans un terminal la commande (composer doit préalablement être installé) : `composer install`
- Générer l'autoload des classes du projet avec la commande : `composer dump-autoload -o`
- Déposer le dossier du connecteur dans le répertoire /data/extensions/ de l'instance Pastell.
- Déclarer l'extension dans l'écran de configuration avec un compte administrateur.
- Paramétrer le connecteur avec les paramètres de connexion de votre serveur Solr, connexion à votre serveur de fichiers (protocole WebDAV ou SFTP par défaut) et vos identifiants Insee Sirene V3.

## Configuration
À partir du formulaire d'administration du connecteur, remplir les champs suivants pour la configuration :

- URL du site OpenData (uploaddata_url) : Par exemple, opendata.girondenumerique.fr (sans http:// ou https://)
- Login (uploaddata_login) : Compte utilisateur autorisé à uploader dans le serveur OpenData
- Mot de passe (uploaddata_password) : Mot de passe du compte utilisateur autorisé à uploader dans le serveur OpenData
- Répertoire VHost (uploaddata_vhost) : Répertoire VirtualHost du domaine, exemple : girondenumerique
- Répertoire distant (uploaddata_root) : Répertoire racine distant, exemple : OpenData
- URL du moteur d'indexation (indexeur_url) : Par exemple, solr.girondenumerique.fr (sans http:// ou https://)
- Port du moteur d'indexation (indexeur_port) : 8983 (par défaut)
- Login (indexeur_login) : Compte utilisateur Pastell autorisé à indexer dans solr
- Mot de passe (indexeur_password) : Mot de passe du compte utilisateur Pastell autorisé à indexer dans solr
- Noyau de documents (indexeur_core) : Nom du noyau de document (attention à la casse)
- Clé API Insee sirene (insee_key) : Clé hashé du compte associé à l'API Insee sirene
- Secret API Insee sirene (insee_secret) : Secret hashé du compte associé à l'API Insee sirene

Puis paramétrer les flux studio associés, avec leurs droits associés.
