<?php

require '/data/extensions/citizendata/vendor/autoload.php';
include __DIR__.'/lib/entity/CitizenDocument.class.php';
include __DIR__.'/lib/SireneConsumer.php';
include __DIR__.'/lib/SolrConsumer.php';
//include __DIR__.'/lib/WebdavConsumer.php';
include __DIR__.'/lib/SFTPConsumer.php';
include __DIR__.'/lib/NormalizeString.php';

use phpseclib\Crypt\RSA;

/**
 * Indexeur class for upload document on OpenData server and indexation in solr server.
 *
 * 
 * @license    http://cecill.info/licences/Licence_CeCILL_V2-fr.html  Cecill V2
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 3.0.11
 */ 
class Indexeur extends GEDConnecteur {

	protected $lastError;

	/** Module is activate ? */
	private $isActivate;

	/** OpenData server configuration */
	private $openDataUrl;
	private $openDataLogin;
	private $openDataPassword;
	private $openDataVHost;
	private $openDataDirectory;

	// /** @var  WebdavConsumer */
	// public $fileConsumer;

	/** @var  SFTPConsumer */
	public $fileConsumer;
	const PRIVATE_KEY 	= '/home/www-data/.ssh/id_rsa';
	
	/** Solr server configuration */
	private $solrUrl;
	private $solrPort;
	private $solrLogin;
	private $solrPassword;
	private $solrCore;

	/** @var  SolrConsumer */
	public $solrConsumer;

	/** Sirene account */
	private $sireneKey;
	private $sireneSecret;

	/** @var  SireneConsumer */
	public $sireneConsumer;

	public function setConnecteurConfig(DonneesFormulaire $donneesFormulaire) {
		$this->isActivate 			= $donneesFormulaire->get('indexeur_activate');

		$this->openDataUrl 			= $donneesFormulaire->get('uploaddata_url');
		$this->openDataLogin 		= $donneesFormulaire->get('uploaddata_login');
		$this->openDataPassword 	= $donneesFormulaire->get('uploaddata_password');
		$this->openDataVHost		= $donneesFormulaire->get('uploaddata_vhost');
		$this->openDataDirectory 	= $donneesFormulaire->get('uploaddata_root');
		//$this->fileConsumer 		= new WebdavConsumer($this->openDataUrl, $this->openDataLogin, $this->openDataPassword, $this->openDataDirectory, $this->isActivate);
		$key = new RSA();
		if (!empty($this->openDataPassword)) {
			$key->setPassword('your-secure-password');
		}
        $key->loadKey(file_get_contents(self::PRIVATE_KEY));
		$this->fileConsumer			= new SFTPConsumer(
			$this->isActivate,
			$this->openDataUrl,
			$this->openDataVHost,
			$this->openDataDirectory,
			$this->openDataLogin,
			$key
		);

		$this->solrUrl 				= $donneesFormulaire->get('indexeur_url');
		$this->solrPort 			= $donneesFormulaire->get('indexeur_port');
		$this->solrLogin 			= $donneesFormulaire->get('indexeur_login');
		$this->solrPassword 		= $donneesFormulaire->get('indexeur_password');
		$this->solrCore 			= $donneesFormulaire->get('indexeur_core');
		$this->solrConsumer 		= new SolrConsumer($this->solrUrl, $this->solrPort, $this->solrLogin, $this->solrPassword, $this->solrCore, $this->isActivate);

		$this->sireneKey			= $donneesFormulaire->get('insee_key');
		$this->sireneSecret			= $donneesFormulaire->get('insee_secret');	
		$this->sireneConsumer 		= new SireneConsumer($this->sireneKey, $this->sireneSecret);
	}

	/**
     * Index document as GED action for acte folder type.
	 * 
     *
     * @param DonneesFormulaire $actForm Generic act form object to extract document metadata and files
     * @return array
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @throws UnrecoverableException
     * @throws Exception
     */
    public function send(DonneesFormulaire $actForm) {
		$gedDocumentsId = null;
		/**
		 * Get document from generic act form 
		 * and transfer to citizen data eligible only for : 
		 * 1 - deliberations, 
		 * 2 - regulatory acts
		 * 5 - budget
		 */ 
		$natureAct = $actForm->get('acte_nature');
		if ($natureAct == 1 || $natureAct == 2 || $natureAct == 5) {
			$id_e = $this->getConnecteurInfo()['id_e'];
			$siren =  $GLOBALS['objectInstancier']->EntiteSQL->getSiren($id_e);
			$organization = $this->sireneConsumer->getOrganizationInformations($siren);
	
			if ($organization != null) {
				$act = $this->getDocumentFromForm($actForm, $organization);
				$act->setOrigin('acte_pastell');
				$this->indexDocument($act, false);
				$gedDocumentsId = $this->getGedDocumentsId();
			}
		}

		return $gedDocumentsId;
    }

	/**
     * Get citizen document from a generic act form object, completed with entity informations from Sirene.
	 * 
     *
	 * @param DonneesFormulaire $actForm Generic act form object to extract document metadata and files
	 * @param Organization $organization Entity organization informations
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return CitizenDocument
     */
	public function getDocumentFromForm(DonneesFormulaire $actForm, Organization $organization) {
		$identifier	= $actForm->get('numero_de_lacte');
		$type 		= $actForm->getFieldData('acte_nature')->getValue()[0];
		$date 		= DateTime::createFromFormat('Y-m-d', $actForm->get('date_de_lacte'));
		/**
		 * Determines the destination directory according to the type of act :
		 * 1 - deliberations 					=> 0_Actes_administratifs
		 * 2 - regulatory acts					=> 0_Actes_administratifs
		 * 5 - budget and financial documents	=> 4_Finances_locales
		 */
		$budgetDocument = false;
		switch ($actForm->get('acte_nature')) {
			case 1:
				$destinationDirectory 	= '0_Actes_administratifs';
				break;
			case 2:
				$destinationDirectory 	= '0_Actes_administratifs';
				break;
			case 5:
				$destinationDirectory 	= '4_Finances_locales';
				$budgetDocument = true;
				break;
		}
		$description 			= $actForm->get('objet');
		$classification 		= $actForm->get('classification');
		
		$files = array();
		// File types exclusion...
		$documentTypeExclusion = array('aractes', 'autre_document_attache', 'annexes_tamponnees', 'bordereau', 'type_piece_fichier', 'signature', 'document_signe', 'iparapheur_historique');
		foreach ($actForm->getAllFile() as $formFiles) {
			$index = 0;
			if (!in_array($formFiles, $documentTypeExclusion)) {
				foreach ($actForm->get($formFiles) as $number => $filename) {
					$filepath = $actForm->getFilePath($formFiles, $number);
					if (file_exists($filepath)) {
						$newFile = new DocumentFile(NormalizeString::normalize($filename), $filepath);
						$newFile->setTypology($actForm->get('type_acte'));
						// Add only "acte_tamponne" file except XML budget file
						if ($formFiles != 'arrete') {
							array_push($files, $newFile);
						} 
						// For XML budget file, add "arrete" file and generate PDF !
						else if ($budgetDocument && pathinfo($newFile->getName(), PATHINFO_EXTENSION) == 'xml') {
							$cacheFolder = dirname(__FILE__).'/cache/';
							if (is_dir($cacheFolder)) {
								if ('777' == substr(sprintf('%o', fileperms($cacheFolder)), -3)) {
									$newFile->setName(NormalizeString::normalize($description).'.xml');
									array_push($files, $newFile);
									$xml = new DomDocument();
									$xml->load($newFile->getPath());
									if ($xml->getElementsByTagName('DocumentBudgetaire')->length != 0) {
										$budgetFilename = pathinfo($newFile->getName(), PATHINFO_FILENAME).'.pdf';
										$budgetFilePath = $cacheFolder.$budgetFilename;

										$printCompJar = dirname(__FILE__).'/module/print-comp.jar';
										if ('777' == substr(sprintf('%o', fileperms($printCompJar)), -3)) {
											exec('/opt/java/bin/java -jar '.$printCompJar.' '.$newFile->getPath().' '.$budgetFilePath, $executionOutput);
											if (file_exists($budgetFilePath)) {
												$budgetFile = new DocumentFile($budgetFilename, $budgetFilePath);
												$budgetFile->setTypology($actForm->get('type_acte'));
												array_push($files, $budgetFile);
											}
										} else {
											throw new Exception('Le composant print-comp.jar ne possède pas les droits d\'exécution : '.substr(sprintf('%o', fileperms($printCompJar)), -3));
										}
									}
								} else {
									throw new Exception('Le répertoire de cache '.$cacheFolder.' n\'a pas les droits suffisants : '.substr(sprintf('%o', fileperms($cacheFolder)), -3));
								}
							} else {
								throw new Exception('Le répertoire de cache '.$cacheFolder.' pour la génération des documents budgétaires au format PDF n\'existe pas');
							}
						}
						
						// Necessary only for attachments (in exclusion list)...
						if ($formFiles == 'autre_document_attache') {
							$typeAttachmentArray = json_decode($actForm->get('type_pj'));
							// Document typology appeared since Pastell 2.0.x
							if ($typeAttachmentArray) {
								$newFile->setTypology($typeAttachmentArray[$index]);
							}
							$index++;
							array_push($files, $newFile);
						}
					}
				}
			}
		}

		$document = new CitizenDocument($description, $destinationDirectory, $files);
		if (!empty($date)) {
			$document->setDate($date);
		}
		$document->setIdentifier($identifier);
		$document->setType($type);
		$document->setClassification($classification);
		$document->setOrganization($organization);

		return $document;
	}

	/**
     * Push documents into a specific directory on OpenData server, 
	 * then index documents with their url and metadata to the Solr server.
	 * 
     *
	 * @param CitizenDocument $document Document with its metadata and its files
	 * @param boolean $deleteFile Flag to delete file after indexation
	 * @param boolean $yearSubDirectory Flag to check/create sub-directory by year
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     */
	public function indexDocument(CitizenDocument $document, bool $deleteFile, $yearSubDirectory = true) {
		$success = false;
		if ($document->getOrganization() != NULL) {				
			if (sizeOf($document->getFiles()) > 0) {
				foreach ($document->getFiles() as $file) {
					// Verify with hash if the document has already been indexed in Solr
					if (!$this->solrConsumer->documentIsAlreadyIndexed($document->getOrganization()->getSiren(), $file->getHash())) {
						// Upload document on OpenData server
						$fileUrl = $this->fileConsumer->uploadDocumentFile($document, $file, $yearSubDirectory);
						if ($fileUrl != NULL) {
							$file->setOpenDataUrl($fileUrl);
							
							// Document indexation on Solr server
							try {
								if ($this->solrConsumer->indexDocumentFile($document, $file)) {
									if ($deleteFile) {
										// At the end, delete the file on Pastell workspace. Keep only .yml metadata file
										$this->deleteFile($file);
									}
									if ($file->isDeleteFile()) {
										$this->deleteFile($file);
									}
									$success = true;
								} else {
									$this->deleteFileFromOpenData($fileUrl, $file->getName());
									$success = false;
									break;
								}
							} catch (Exception $e) {
								$this->deleteFileFromOpenData($fileUrl, $file->getName(), $e->getMessage());
								$success = false;
								break;
							}
						} else {
							$this->lastError = 'Le document '.$file->getName().' n\'a pas été déposé sur le serveur OpenData';
							$success = false;
							break;
						}
					} else {
						// Document has already been indexed, set treatment as successfull to flag document as Exported.
						$this->lastError = 'Le document '.$file->getName().' a déjà été versé dans les données citoyennes';
						$success = true;
					}
				}
			} else {
				$this->lastError = 'Aucun document à indexer';
				$success = true;
			}
		} else {
			$this->lastError = 'Les informations de l\'entité n\'ont pas été récupérées';
			$success = false;
		}

		return $success;
	}

	/**
     * Delete a document from Pastell workspace.
	 * 
     *
	 * @param DocumentFile $file Document file to delete
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     */
	private function deleteFile(DocumentFile $file) {
		return unlink($file->getPath());
	}

	/**
     * Delete a document from OpenData server.
	 * 
     *
	 * @param string $fileUrl Document URL for deletion
	 * @param string $filename File name for display message
	 * @param string $message Message to display, empty by default
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     */
	private function deleteFileFromOpenData(string $fileUrl, string $filename, $message = '') {
		if (empty($message)) {
			$this->lastError = 'Le document '.$filename.' n\'a pas été indexé';
		} else {
			$this->lastError = $message;
		}
		
		if ($this->fileConsumer->deleteDocumentFile($fileUrl)) {
			$this->lastError = $this->lastError.'<br />Le fichier a été supprimé du serveur OpenData';
		} else {
			$this->lastError = $this->lastError.'<br />Le fichier '.$filename.' n\'a pas été supprimé du serveur OpenData';
		}
	}
}