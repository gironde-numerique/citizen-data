<?php 

/**
 * Import generic acts action class for export all generic act documents to citizen data.
 *
 *
 * @license    http://cecill.info/licences/Licence_CeCILL_V2-fr.html  Cecill V2
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class ImportGenericActs  extends ActionExecutor {

	const TYPE = 'actes-generique';
	
	public function go() {
		$this->setAction('Publié');

		/** @var Indexeur $indexeur */
        $indexeur = $this->getMyConnecteur();
		
		// Initialization of variables to browse documents
		$offset = 0;
		$limit = 2;
		$documentsNotTerminated = $documentsExported = $documentsAlreadyExported = $documentNonEligible = $documentsInFailure = 0;
		$documentsTotal = $total = intval($this->getDocumentActionEntite()->getNbDocument($this->id_e, self::TYPE, ''));

		if ($total > 0) {
			$siren =  $this->objectInstancier->EntiteSQL->getSiren($this->id_e);
			$organization = $indexeur->sireneConsumer->getOrganizationInformations($siren);
		}
		
		while ($total > 0 && $documentsExported < 30) {
			$documentsList = $this->getDocumentActionEntite()->getListDocument($this->id_e, self::TYPE, $offset, $limit, '', false, 'ASC');
			if ($documentsList != NULL && sizeof($documentsList) > 0) {
				foreach ($documentsList as $doc) {
					$documentId = $doc['id_d'];
					$total--;
					// Export only documents that have completed their workflow
					$documentDetails = $this->getDonneesFormulaireFactory()->get($documentId);
					$actions = $this->getDocumentActionEntite()->getAction($this->id_e, $documentId);
					if ($this->documentCanBeExported($actions, $documentDetails)) {
						/**
						 * Get document from generic act form 
						 * and transfer to citizen data eligible only for : 
						 * 1 - deliberations, 
						 * 2 - regulatory acts
						 * 5 - budget
						 */ 
						$natureAct = $documentDetails->get('acte_nature');
						if ($natureAct == 1 || $natureAct == 2 || $natureAct == 5) {
							$document = $indexeur->getDocumentFromForm($documentDetails, $organization);
							$document->setOrigin('import_pastell');
	
							// Index document only if it contains files
							if (sizeOf($document->getFiles()) > 0) {
								if ($indexeur->indexDocument($document, false)) {
									$this->setDocumentId(self::TYPE, $doc['id_d']);
									$this->addActionOK('Document publié vers les données citoyennes');
									$documentsExported++;
								} else {
									$documentsInFailure++;
								}
							} else {
								$documentNonEligible++;
							}
						} else {
							$documentNonEligible++;
						}
					} else {
						if ($this->isActionAlreadyDone($actions, 'send-ged') || $this->isActionAlreadyDone($actions, 'Publié')) {
							$documentsAlreadyExported++;
							// Update status for document published manually by "actes générique" or automatically by "actes automatiques"
							if ($this->isActionAlreadyDone($actions, 'send-ged')) {
								$this->setDocumentId(self::TYPE, $doc['id_d']);
								$this->addActionOK('Document publié vers les données citoyennes');
							}
						} else {
							$documentsNotTerminated++;
						}
					}
				}
				$offset += $limit;
			}
		}

		// Display citizen data import rapport.
		$this->setLastMessage('Nombre total de documents : '.$documentsTotal);
		if ($documentsNotTerminated > 0) {
			$this->setLastMessage($this->getLastMessage().'<br />Nombre de document(s) n\'ayant pas terminé leur "workflow" : '.$documentsNotTerminated);
		}
		if ($documentsExported > 0) {
			$this->setLastMessage($this->getLastMessage().'<br />Nombre de document(s) publié(s) vers les données citoyennes : '.$documentsExported);
		}
		if ($documentsAlreadyExported > 0) {
			$this->setLastMessage($this->getLastMessage().'<br />Nombre de document(s) déjà publié(s) : '.$documentsAlreadyExported);
		}
		if ($documentNonEligible > 0) {
			$this->setLastMessage($this->getLastMessage().'<br />Nombre de document(s) non éligible(s) aux données citoyennes : '.$documentNonEligible);
		}
		if (($documentsAlreadyExported + $documentNonEligible + $documentsNotTerminated) == $documentsTotal) {
			$this->setLastMessage($this->getLastMessage().'<br /><br /><b>Import vers les données citoyennes terminé.</b>');
		}
		if ($documentsInFailure > 0) {
			$this->setLastMessage($this->getLastMessage().'<br />Nombre de document(s) dont la publication a échoué : '.$documentsInFailure);
			return false;
		} else {
			return true;
		}
	}

	/**
     * Check document state for eligibility to export to citizen data.
	 * 
     *
	 * @param array $actions Document actions history
	 * @param DonneesFormulaire $genericActForm Generic act form object to extract document workflow step
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     */
	private function documentCanBeExported(array $actions, DonneesFormulaire $genericActForm) {
		$state = true;
		if ($this->isActionAlreadyDone($actions, 'Publié')) {
			$state = false;
		} else {
			$envoi_tdt 				= $genericActForm->get('envoi_tdt');
			$envoi_ged 				= $genericActForm->get('envoi_ged');
			$envoi_sae 				= $genericActForm->get('envoi_sae');

			if ($envoi_tdt && !$this->isActionAlreadyDone($actions, 'acquiter-tdt')) {
				$state = false;
			}
			if ($envoi_ged && $this->isActionAlreadyDone($actions, 'send-ged')) {
				$state = false;
			}
			if ($envoi_sae && !$this->isActionAlreadyDone($actions, 'accepter-sae')) {
				$state = false;
			}
		}
		
		return $state;
	}

	/**
     * Check if an action has been already done.
	 * 
     *
	 * @param array $actions Document actions history
	 * @param string $documentId Document id
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     */
	private function isActionAlreadyDone(array $actions, string $actionName) {
		foreach ($actions as $action){
            if ($action['action'] == $actionName){
				return true;
            }
		}
		return false;
	}
}