<?php

/**
 * UploadData connection action class for testing connection to an OpenData server.
 *
 *
 * @license    http://cecill.info/licences/Licence_CeCILL_V2-fr.html  Cecill V2
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class UploadDocumentTest extends ActionExecutor {
	
	public function go() {
		/** @var Indexeur $indexeur */
		$indexeur = $this->getMyConnecteur();

		if ($indexeur->fileConsumer->testConnection()) {
			$this->setLastMessage('<b>1.</b> Connexion au serveur OpenData réussie.');

			$documentFile = new DocumentFile(NormalizeString::normalize('TEST.pdf'), __DIR__.'/documents/TEST.pdf');

			$document = new CitizenDocument('Test indexation', '', array($documentFile));
			$document->setDate(new DateTime('NOW'));
			$document->setIdentifier('TEST00000033');
			
			$organization = new Organization('SYNDICAT MIXTE GIRONDE NUMERIQUE', '200010049', '76');
			$address = new Address();
			$address->setStreet('8 RUE CORPS FRANC POMMIES');
			$address->setComplement('IMM GIRONDE-REZ DALLE-TERR 08/05/45');
			$address->setZipcode('33000');
			$address->setCity('Bordeaux');
			$organization->setAddress($address);
			$document->setOrganization($organization);

			$fileUrl = $indexeur->fileConsumer->uploadDocumentFile($document, $documentFile, false);
			if ($fileUrl != NULL) {
				$this->setLastMessage($this->getLastMessage().'<br /><br /><b>2.</b> Le document <a target="_blank" href="'.$fileUrl.'">'.$documentFile->getName().'</a> a bien été déposé.');

				if ($indexeur->fileConsumer->deleteDocumentFile($fileUrl)) {
					$this->setLastMessage($this->getLastMessage().'<br /><br /><b>3.</b> Document de test supprimé.');
					return true;
				} else {
					$this->setLastMessage($this->getLastMessage().'<br /><br /><b>3.</b> Echec de la suppression du document de test.');
					return false;
				}
			} else {
				$this->setLastMessage($this->getLastMessage().'<br /><br /><b>2.</b> Echec du dépôt du document de test '.$documentFile->getName().'.');
				return false;
			}
		} else {
			$this->setLastMessage($indexeur->getLastError());
			return false;
		}
	}
  
}
