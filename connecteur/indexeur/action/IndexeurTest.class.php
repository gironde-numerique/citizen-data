<?php

/**
 * Indexeur test action class for testing a document indexation in a solr server.
 *
 *
 * @license    http://cecill.info/licences/Licence_CeCILL_V2-fr.html  Cecill V2
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class IndexeurTest extends ActionExecutor {

    public function go() {
        /** @var Indexeur $indexeur */
		$indexeur = $this->getMyConnecteur();

		// Ping Solr server
		if ($indexeur->solrConsumer->testConnection()) {
			$this->setLastMessage('Connexion au serveur Solr réussie.<br />Version du client Solr : '.SolrUtils::getSolrVersion());
			
			$documentFile = new DocumentFile('TEST.pdf', __DIR__.'/documents/TEST.pdf');
			$documentFile->setOpenDataUrl('http://opendata.girondenumerique.fr/path/TEST.pdf');

			$document = new CitizenDocument('Test indexation', '', array($documentFile));
			$document->setDate(new DateTime('NOW'));
			$document->setIdentifier('TEST00000033');
			
			$organization = new Organization('SYNDICAT MIXTE GIRONDE NUMERIQUE', '200010049', '76');
			$address = new Address();
			$address->setStreet('8 RUE CORPS FRANC POMMIES');
			$address->setComplement('IMM GIRONDE-REZ DALLE-TERR 08/05/45');
			$address->setZipcode('33000');
			$address->setCity('Bordeaux');
			$organization->setAddress($address);
			$document->setOrganization($organization);
			
			// First, index document TEST.pdf
			if ($indexeur->solrConsumer->indexDocumentFile($document, $documentFile)) {
				$this->setLastMessage($this->getLastMessage().'<br /><br /><b>1.</b> Indexation d\'un document de test réussi.');

				// Then, search document TEST.pdf in Solr
				$criterias = array(
					'documentidentifier'    => 'TEST00000033',
					'siren'         		=> '200010049',
					'ville'         		=> '‎Bordeaux',
					'codepostal'    		=> 33000
				);
				$searchResponse = $indexeur->solrConsumer->searchDocument('', $criterias);
				if (isset($searchResponse) && $searchResponse['response']['numFound'] > 0) {
					$documentId = $searchResponse['response']['docs'][0]->id;
					$this->setLastMessage($this->getLastMessage().'<br /><br /><b>2.</b> Document retourné après recherche (identifiant : '.$documentId.').');
					
					// Finally, delete document index with its id
					if ($indexeur->solrConsumer->deleteDocumentIndexById($documentId)) {
						$this->setLastMessage($this->getLastMessage().'<br /><br /><b>3.</b> Document de test supprimé.');
						return true;
					} else {
						$this->setLastMessage($this->getLastMessage().'<br /><br /><b>3.</b> Le document de test n\'a pas été supprimé de l\'index.');
						return false;
					}
				} else {
					$this->setLastMessage($this->getLastMessage().'<br /><br /><b>2.</b> Impossible de retrouver le document indexé.');
					return false;
				}
			} else {
				$this->setLastMessage($this->getLastMessage().'<br /><br />L\'indexation du document de test a échoué<br />'.$indexeur->getLastError());
				return false;
			}
		} else {
			$this->setLastMessage($indexeur->getLastError());
			return false;
		}
    }

}
