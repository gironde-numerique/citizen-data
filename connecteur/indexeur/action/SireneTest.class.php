<?php

/**
 * Sirene test action class for testing SIREN recovering and Sirene API calling.
 *
 *
 * @license    http://cecill.info/licences/Licence_CeCILL_V2-fr.html  Cecill V2
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class SireneTest extends ActionExecutor {

    public function go() {
        /** @var Indexeur $indexeur */
		$indexeur = $this->getMyConnecteur();
		
		$version = $indexeur->sireneConsumer->getSireneStatus();
		if (!empty($version)) {
			$this->setLastMessage('Connexion à l\'API Sirene réussie. Version du service '.$version);

			$siren =  $this->objectInstancier->EntiteSQL->getSiren($this->id_e);
			if (!empty($siren)) {
				$informations = $indexeur->sireneConsumer->getOrganizationInformations($siren);
				if ($informations != NULL) {
					$this->setLastMessage($this->getLastMessage().'<br /><br /><b>'.$informations->getName().'</b> (SIRET '.$informations->getSiren().$informations->getNic().')');
					if (!empty($informations->getAddress()->getStreet())) {
						$this->setLastMessage($this->getLastMessage().'<br />'.$informations->getAddress()->getStreet());
					}
					if (!empty($informations->getAddress()->getPostbox())) {
						$this->setLastMessage($this->getLastMessage().'<br />'.$informations->getAddress()->getPostbox());
					}
					$this->setLastMessage($this->getLastMessage().'<br />'.$informations->getAddress()->getZipcode().' '.$informations->getAddress()->getCity());
					if (!empty($informations->getAddress()->getCedexcode())) {
						$this->setLastMessage($this->getLastMessage().' CEDEX '.$informations->getAddress()->getCedexcode());
					}
					return true;
				} else {
					$this->setLastMessage($this->getLastMessage().'<br /><br />Impossible de récupérer les informations l\'entité '.$siren.' ('.$this->id_e.')');
					return false;
				}
			} else {
				$this->setLastMessage($this->getLastMessage().'<br /><br />Impossible de récupérer le SIREN pour l\'entité pour l\'identifiant '.$this->id_e);
				return false;
			}
		} else {
			$this->setLastMessage($indexeur->getLastError());
			return false;
		}
    }

}
