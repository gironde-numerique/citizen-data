<?php

/**
 * SolrConsumer class to interact with Solr server.
 *
 * 
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class SolrConsumer {

    /** Solr server configuration */
	private $url;
	private $port;
	private $login;
	private $password;
	private $core;

	/** Solr API url for cURL */
	private $api;

	/** Solr API method */
    const INDEX_FILE = '/update/extract';
    
    /** Module is activate ? */
    private $isActivate;

    public function __construct($url, $port, $login, $password, $core, $isActivate) {
        $this->url 			= $url;
		$this->port 		= $port;
		$this->login 		= $login;
		$this->password 	= $password;
        $this->core 		= $core;
        $this->isActivate   = $isActivate;

		$this->api 			= 'http://'.$this->url.':'.$this->port.'/solr/'.$this->core;
	}

    /**
	 * Index a document in Solr with custom field values. 
	 * Complex document must be indexed through cURL request, this functionnality is not implemented yet in PHP Solr client.
	 *
	 * @param CitizenDocument $document Document to index with its metadata
	 * @param DocumentFile $file File to index with its open data URL
	 * @throws Exception If curl call generate an error
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 */
	public function indexDocumentFile(CitizenDocument $document, DocumentFile $file) {
		$cFile = new CURLFile($file->getPath(), $file->getFormat(), $file->getName());

		// Basic configuration for writing in Solr index server
        $postConfiguration = array(
            'commitWithin'  => '1000',
            'overwrite'     => 'true',
            'wt'            => 'json',
            'commit'        => 'true'
        );
        
        $ch = curl_init();
        $options = array(
			CURLOPT_URL             => $this->api.self::INDEX_FILE,
			CURLOPT_ENCODING		=> '',
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
			CURLOPT_HEADER          => 0,
			CURLINFO_HEADER_OUT		=> true,
            CURLOPT_POST            => 1,
            CURLOPT_HTTPHEADER      => array('Content-Type: multipart/form-data', 'Authorization: Basic '.base64_encode($this->login.':'.$this->password)),
            CURLOPT_POSTFIELDS      => array_merge($postConfiguration, $this->setMetadataArray($document, $file), array('myFile' => $cFile))
		);
        curl_setopt_array($ch, $options);
		curl_exec($ch);
		$result = false;
		$errmsg;
        if (curl_errno($ch) == 0) {
			$status = curl_getinfo($ch)['http_code'];
            if ($status == 200) {
                $result = true;
            } else if ($status == 401) {
				throw new Exception('Vous n\'êtes pas autorisé à accéder au serveur d\'indexation, veuillez vérifier vos identifiants de connexion');
			}
        } else {
			$errmsg = curl_error($ch);
		}
		curl_close($ch);

		// Only if curl generate an error, throw exception.
		if (!$result && isset($errmsg)) {
			throw new Exception($errmsg);
		}
		
		return $result;
	}

	/**
	 * Search documents in Solr server from search criteria.
	 *
	 * @param string $keywords Optional search keywords
	 * @param array $criteria Optional search criterias field => value
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrQueryResponse->SolrObject
	 */
	public function searchDocument(string $keywords, array $criterias) {
		$query = new SolrQuery();

		// Main keywords
		if (is_null($keywords) || empty($keywords)) {
			$keywords = '*:*';
		}
		$query->setQuery($keywords);
		
		// Criteria by filter field
		if (sizeof($criterias) > 1) {
			foreach($criterias as $field => $value){
				$query->addFilterQuery($field.':'.$value);
			}
		}

		return $this->getSolrClient()->query($query)->getResponse();
	}

	/**
	 * Check if a document has already been indexed in Solr with its file hash.
	 *
	 * @param string $siren Entity Siren as index filter
	 * @param string $hash File hash
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 */
	public function documentIsAlreadyIndexed(string $siren, string $hash) {
		$response = $this->searchDocument('', array('siren' => $siren, 'hash' => $hash));

		if (isset($response) && $response['response']['numFound'] > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Delete a document index in Solr by its solr document id.
	 *
	 * @param string $solrDocumentId Solr document id
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 */
	public function deleteDocumentIndexById(string $solrDocumentId) {
		$deleteResponse = $this->getSolrClient()->deleteById($solrDocumentId);
		$commitResponse = $this->getSolrClient()->commit();
		
		// Check delete and commit request
		if ($deleteResponse->getHttpStatus() == 200 
			&& $commitResponse->getHttpStatus() == 200) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Test a connection to the solr server.
	 *
	 * @throws Exception If connection test fail
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 */
	public function testConnection() {
		try {
			if ($this->getSolrClient()->ping()) {
				return true;
			}
		} catch (SolrClientException $e) {
			$error = $e->getMessage();
			if (strpos($e->getMessage(), 'Error 6')) {
				$error = 'Impossible de résoudre le nom d\'hôte';
			} else if (strpos($e->getMessage(), 'Error 3')) {
				$error = 'Mauvais format d\'URL ou URL manquante';
			} else if (strpos($e->getMessage(), 'Code 404')) {
				$error = 'Noyau de documents introuvable';
			} else if (strpos($e->getMessage(), '401')) {
                throw new Exception('Echec de l\'authentification Solr');
            } else if (strpos($e->getMessage(), '403')) {
                throw new Exception('Vous n\'êtes pas authorisé à accèder au serveur Solr');
            }
            
			throw new Exception('Erreur : '.$error);
		}
	}

	/**
     * Set a metadata array with all document informations for Solr indexation.
	 * 
     *
	 * @param CitizenDocument $document Document to index with its metadata
	 * @param DocumentFile $file File to index with its open data URL
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return array
     */
	private function setMetadataArray(CitizenDocument $document, DocumentFile $file) {
		$insertionDate = new DateTime();
		$fieldValues = array(
			'literal.hash'					=> $file->getHash(),
			'literal.filepath'      		=> $file->getOpenDataUrl(),
			'literal.description'      		=> $document->getDescription(),
			'literal.creationdate'			=> $creationDate = $insertionDate->format('Y-m-d').'T00:00:00Z',
			'literal.date'					=> $date = $document->getDate()->format('Y-m-d').'T00:00:00Z',
			'literal.origin'				=> $document->getOrigin(),
			'literal.entity'        		=> $document->getOrganization()->getName(),
			'literal.siren'         		=> $document->getOrganization()->getSiren(),
			'literal.nic'         			=> $document->getOrganization()->getNic(),
			'literal.adresse1'      		=> $document->getOrganization()->getAddress()->getStreet(),
			'literal.adresse2'      		=> $document->getOrganization()->getAddress()->getComplement(),
			'literal.ville'         		=> $document->getOrganization()->getAddress()->getCity(),
			'literal.codepostal'    		=> $document->getOrganization()->getAddress()->getZipcode(),
			'literal.boitepostale'    		=> $document->getOrganization()->getAddress()->getPostbox(),
			'literal.cedex'    				=> $document->getOrganization()->getAddress()->getCedexcode(),
			'literal.categorie'    			=> $document->getOrganization()->getCategory(),
			'literal.categorieJuridique'    => $document->getOrganization()->getLegalCategory(),
			'literal.activite'    			=> $document->getOrganization()->getActivity(),
			'literal.nomenclatureActivite'  => $document->getOrganization()->getActivityNomenclature()
		);

		// These following values can be NULL, not necessary to save them as empty
		if (!empty($document->getIdentifier())) {
			$fieldValues['literal.documentIdentifier'] = $document->getIdentifier();
		}
		if (!empty($document->getType())) {
			$fieldValues['literal.documentType'] = $document->getType();
		}
		if (!empty($document->getClassification())) {
			$fieldValues['literal.classification'] = $document->getClassification();
		}
		if (!empty($file->getTypology())) {
			$fieldValues['literal.typology'] = $file->getTypology();
		}

		return $fieldValues;
	}

	/**
	 * Get solr client if module is activate
	 *
	 * @throws Exception If module is deactivate
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return SolrClient
	 */
	protected function getSolrClient() {
		if (!$this->isActivate) {
			throw new Exception('Le module n\'est pas activé');
		} else {
			$config = array (
				'hostname' => $this->url,
				'login'    => $this->login,
				'password' => $this->password,
				'port'     => $this->port,
				'timeout'  => 10,
				'path'     => '/solr/'.$this->core
			);

			return new SolrClient($config);
		}
	}
}