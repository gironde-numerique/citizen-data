<?php

/**
 * SireneConsumer class to consume Insee Sirene API.
 *
 * 
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class SireneConsumer {

    /** Authorization bearer for Sirene API */
    private $key;
    private $secret;
    private $token;

    /** Insee API */
    private $inseeApi = 'https://api.insee.fr';
    const TOKEN = '/token';

    /** Insee Sirene API */
    private $sireneApi = 'https://api.insee.fr/entreprises/sirene/V3';
    const SIRENE_STATUS = '/informations';
    const SIRENE_INFORMATIONS_BY_SIREN = '/siret';

    public function __construct($key, $secret) {
        $this->key      = $key;
        $this->secret   = $secret;

        // At initialization, get or retrive the authorization bearer.
        $this->token = $this->getAuthorizationToken();
    }

    /**
     * Get organization informations for a specific Siren. 
     * This function returns the most recent informations if the organization has an history
     *
     * @param string $siren Organization SIREN
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return object
     */
    public function getOrganizationInformations($siren) {
        try {
            $params = 'q=siren:'.$siren;
            $response = $this->apiCurlCalling($this->sireneApi.self::SIRENE_INFORMATIONS_BY_SIREN, $this->token, $params);

            $organization = NULL;
            if ($response != NULL && isset($response->header) && $response->header->statut == 200) {
                // If organization has no history, get directly the element from response
                if (sizeof($response->etablissements) == 1) {
                    $organization = $response->etablissements[0];
                } 
                // Else get the most recent...
                else {
                    foreach ($response->etablissements as $organizationTmp) {
                        if ($organization == NULL) {
                            $organization = $organizationTmp;
                        } else if (strtotime($organizationTmp->dateCreationEtablissement) > strtotime($organization->dateCreationEtablissement)) {
                            $organization = $organizationTmp;
                        }
                    }
                }
                
                return $this->extractOrganizationInformations($organization);

            } else if ($response != NULL && isset($response->fault) && $response->fault->code == 900804) {
                // Sirene API calls limit exceeded, must wait 60 sec
                sleep(60);
                $this->getOrganizationInformations($siren);
            }
        } catch (Exception $e) {
            throw new Exception('Erreur : Impossible de récupérer les informations de l\'entité pour le SIREN '.$siren);
        }
    }

    /**
	 * Get Sirene API status with by returning the version number.
	 *
	 * @throws Exception If curl call generate an error
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 */
	public function getSireneStatus() {
        try {
            $response = $this->apiCurlCalling($this->sireneApi.self::SIRENE_STATUS, $this->token);
            return $response->versionService;
        } catch (Exception $e) {
            throw new Exception('Erreur : API Insee Sirene injoignable');
        }
    }
    
    /**
     * Get authorization bearer from consumer key and secret.
     *
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     */
    private function getAuthorizationToken() {
        try {
            $response = $this->apiCurlCalling($this->inseeApi.self::TOKEN, NULL, 'grant_type=client_credentials&validity_period=86400');
            return $response->access_token;
        } catch (Exception $e) {
            throw new Exception('Erreur : Impossible de récupérer le jeton d\'authorisation Sirene');
        }
    }

    /**
     * Execute a cURL call and return JSON response as object.
     *
     * @param string $url Insee API service URL
     * @param string $token Authorization bearer get from Insee account
     * @param string $params URL formed query params
     * @throws Exception If curl call generate an error
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return object
     */
    private function apiCurlCalling($url, $token = NULL, $params = NULL) {
        $errmsg = NULL;
        $ch = curl_init();
        $options = array(
            CURLOPT_URL             => $url,
            CURLOPT_HEADER          => 0,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_RETURNTRANSFER  => true
        );

        if ($token != NULL) {
            $options[CURLOPT_HTTPHEADER] = array(
                'Accept: application/json', 
                'Authorization: Bearer '.$token
            );
        } else {
            // If token is NULL, basic authorization with consumer key and secret to get token.
            $options[CURLOPT_HTTPHEADER] = array(
                'Accept: application/json', 
                'Authorization: Basic '.base64_encode($this->key.':'.$this->secret)
            );
        }

        if ($params != NULL) {
            $options[CURLOPT_POSTFIELDS] = $params;
        }

        curl_setopt_array($ch, $options);
        $response = json_decode(curl_exec($ch));
        if (curl_errno($ch) != 0) {
            $errmsg = curl_error($ch);
        }
        curl_close($ch);

        if ($errmsg != NULL) {
            throw new Exception($errmsg);
        } else if ($response == NULL) {
            throw new Exception('Erreur : L\'appel n\'a généré aucune réponse');
        }

        return $response;
    }

    /**
     * Extract organization useful informations from JSON response
     *
     * @param stdClass $establishment Establishment from JSON response
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return object
     */
    private function extractOrganizationInformations(stdClass $establishment) {
        $organizationInformations = new Organization($establishment->uniteLegale->denominationUniteLegale, $establishment->siren, $establishment
        ->nic);
                
        $address = new Address();
        $addressLine1 = $establishment->adresseEtablissement->numeroVoieEtablissement.' '.$establishment->adresseEtablissement->typeVoieEtablissement.' '.$establishment->adresseEtablissement->libelleVoieEtablissement;
        if (!empty($addressLine1)) {
            $address->setStreet(trim($addressLine1));
        }
        $addressComplement = $establishment->adresseEtablissement->complementAdresseEtablissement;
        if (!empty($addressComplement)) {
            $address->setComplement($addressComplement);
        }
        $postbox = $establishment->adresseEtablissement->distributionSpecialeEtablissement;
        if (!empty($postbox)) {
            $address->setPostbox($postbox);
        }
        $zipcode = $establishment->adresseEtablissement->codePostalEtablissement;
        if (!empty($zipcode)) {
            $address->setZipcode($zipcode);
        }
        $city = $establishment->adresseEtablissement->libelleCommuneEtablissement;
        if (!empty($city)) {
            $address->setCity($city);
        }
        $cedex = $establishment->adresseEtablissement->codeCedexEtablissement;
        if (!empty($cedex)) {
            $address->setCedexcode($cedex);
        }
        $organizationInformations->setAddress($address);

        $category = $establishment->uniteLegale->categorieEntreprise;
        if (!empty($category)) {
            $organizationInformations->setCategory($category);
        }
        $legalCategory = $establishment->uniteLegale->categorieJuridiqueUniteLegale;
        if (!empty($legalCategory)) {
            $organizationInformations->setLegalCategory($legalCategory);
        }
        $activity = $establishment->uniteLegale->activitePrincipaleUniteLegale;
        if (!empty($activity)) {
            $organizationInformations->setActivity($activity);
        }
        $activityNomenclature = $establishment->uniteLegale->nomenclatureActivitePrincipaleUniteLegale;
        if (!empty($activityNomenclature)) {
            $organizationInformations->setActivityNomenclature($activityNomenclature);
        }
        
        return $organizationInformations;
    }
}