<?php

include 'Sabre/autoload.php';

/**
 * WebdavConsumer class to upload file on OpenData server with WebDAV protocol.
 *
 * 
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class WebdavConsumer {

    /** Opendata server configuration */
    private $url;
	private $login;
    private $password;
    private $rootFolder;
    private $baseUri;
    private $openDataUrl;

    /** Module is activate ? */
    private $isActivate;

    /** WebDav explorer path */
    const WEBDAV_PATH = 'extplorer/webdav.php';

    public function __construct($url, $login, $password, $rootFolder, $isActivate) {
        $this->url          = $url;
        $this->login        = $login;
        $this->password     = $password;
        $this->rootFolder   = $rootFolder;
        $this->isActivate   = $isActivate;

        $this->baseUri = $this->url.'/'.self::WEBDAV_PATH.'/'.$this->rootFolder.'/';
        $this->openDataUrl = 'http://'.$this->url.'/'.$this->rootFolder.'/';
    }

    /**
     * Delete a file for an absolute file URL. Determines the directory from the file URL.
	 * 
     *
     * @param string $directory Distant directory into file must be deleted
	 * @param string $filename Document filename to delete
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     */
    public function deleteDocumentFile(string $fileUrl) {
        $filename = basename($fileUrl);
        $directory = str_replace($filename, '', parse_url($fileUrl, PHP_URL_PATH));         // Delete file name from path
        $directory = str_replace($this->rootFolder, '', $directory);                        // Delete root folder to get only destination directory
        $directory = ltrim($directory, '/');                                                // Delete first "/"

        $response = $this->getWebDAVClient($directory)->request('DELETE', $filename);
        $status = $response['statusCode'];
        if ($status == 200 || $status == 204) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Upload document file into a specific directory on OpenData server. 
     * Use PUT stream cURL.
	 * 
     *
     * @param CitizenDocument $document Document and its informations
	 * @param DocumentFile $file File to upload
     * @param boolean $yearSubDirectory Flag to check/create sub-directory by year
     * @throws Exception If document does not exist
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     */
    public function uploadDocumentFile(CitizenDocument $document, DocumentFile $file, bool $yearSubDirectory) {
        $fileUrl = NULL;
        $errmsg = NULL;
        $directory = '';
        if ($document->getDestinationDirectory() == ' aucun') {
            $directory = '';
        } else if (!empty($document->getDestinationDirectory())) {
            $directory = $document->getDestinationDirectory().'/';
        }

        // Check/create sub-directory if year has been fullfilled
        if ($yearSubDirectory) {
            $directory = $this->createSubDirectoryForYear($directory, $document->getDate());
        }

        $fileHandle = fopen($file->getPath(), 'r');
        if (!$fileHandle) {
            throw new Exception('Impossible d\'ouvrir le fichier '.$file->getPath());
        } else {
            $ch = curl_init();
            $options = array(
                CURLOPT_URL             => $this->baseUri.$directory.$file->getName(),
                CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                CURLOPT_PUT             => true,
                CURLOPT_INFILE          => $fileHandle,
                CURLOPT_INFILESIZE      => filesize($file->getPath()),
                CURLOPT_HTTPAUTH        => CURLAUTH_BASIC,
                CURLOPT_USERPWD         => $this->login.':'.$this->password
            );
            curl_setopt_array($ch, $options);
            curl_exec($ch);
            if (curl_errno($ch) == 0) {
                $status = curl_getinfo($ch);
                if ($status['http_code'] == 201) {
                    $fileUrl = $this->openDataUrl.$directory.$file->getName();
                }
            } else {
                $errmsg = curl_error($ch);
            }
            curl_close($ch);
    
            // Finally
            fclose($fileHandle);
    
            if ($errmsg != NULL) {
                throw new Exception($errmsg);
            }
    
            return $fileUrl;
        }
    }

    /**
	 * Get OpenData directory tree structure from the connector configuration.
	 *
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return array
	 */
    public function getDirectoryStructure() {
        $structure = Array();
        $this->exploreDirectory('', 1, $structure);
  
        return $structure;
    }

    /**
	 * Test a connection to the opendata server.
	 *
     * @throws Exception If connection test fail
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 */
	public function testConnection() {
        try {
            $response = $this->getWebDAVClient()->request('GET');

            if ($response['statusCode'] == 200) {
                return true;
            } else {
                throw new Exception('Erreur : Impossible de se connecter au serveur OpenData');
            }
        } catch (Sabre\HTTP\ClientException $e) {
            if ($e->getCode() == 6) {
                $error = 'Erreur : Impossible de résoudre l\'URL';
            } else {
                $error = $e->getMessage();
            }
            throw new Exception($error);
        }
    }

    /**
	 * Search and create if not exists a sub-directory as year for a specific top directory.
     * If exists or created, this function return the relative path to the sub-directory.
	 *
     * @param string $topDirectory Top directory within must be a sub-directory as year
     * @param DateTime $documentDate Document date for year to search or create
     * @throws Exception If sub-directory does not exist and cannot be created
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 */
    private function createSubDirectoryForYear(string $topDirectory, DateTime $documentDate) {
        $yearSubDirectory = $documentDate->format('Y');

        $response = $this->getWebDAVClient($topDirectory)->request('GET', '');
        // Warning, href use simple quote !
        $subDirectoryExists = preg_match("|<a href='".$yearSubDirectory."'>".$yearSubDirectory."</a>|", $response['body']);
        if ($subDirectoryExists != 1) {
            $createSubDirectoryRequest = $this->getWebDAVClient($topDirectory)->request('MKCOL', $yearSubDirectory);
            if ($createSubDirectoryRequest['statusCode'] != 201 ) {
                throw new Exception('Impossible de créer le sous-répertoire '.$yearSubDirectory.'. Statut : '.$createSubDirectoryRequest['statusCode']);
            }
        }
        // Whatever the year sub-directory is concat to the destination directory
        return $topDirectory.$yearSubDirectory.'/';
    }

    /**
	 * Explore recursively a WebDAV directory.
	 *
     * @param string $path Distant directory path
     * @param string $level Level to explore (maximum 2)
     * @param array $directoryStructure Directory structure in array
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return int
	 */
    private function exploreDirectory($path, $level, &$directoryStructure) {
        if ($level > 2) { return 0; }
        try {
            $directoryElements = $this->getWebDAVClient($path)->propfind('', array(
                '{DAV:}displayname',
                '{DAV:}getcontenttype',
                '{DAV:}getcontentlength',
            ), 1);
        } catch(Exception $e) {
            return 0;
        }
    
        // for the top tree, force the node to be displayed
        if ($path == '') {
            $topDirectory = array(
                'name'  => '-AUCUN- (répertoire racine)',
                'path'  => '/',
                'level' => $level
            );
            $directoryStructure[] = $topDirectory;
        }
    
        ksort($directoryElements);
        $count = 0;
        $index = 0;
        foreach ($directoryElements as $elementPath => $elementInfo) {
            if ($elementInfo['{DAV:}getcontenttype'] == 'httpd/unix-directory') {
                // Assume 1st element is the top directory ?!
                if ($index == 0) {
                    $index += 1;
                    continue;
                } 

                $directoryTmp = array(
                    'name' => $elementInfo['{DAV:}displayname'],
                    'path' => $path.'/'.$elementInfo['{DAV:}displayname'],
                    'level' => $level
                );
                $directoryStructure[] = $directoryTmp;
                $this->exploreDirectory($directoryTmp['path'], $level + 1, $directoryStructure);

                $index += 1;
                $count += 1;
            }
        }

        return($count);
    }

    /**
	 * Get WebDAV client if module is activate
	 *
     * @param string $directoryPath Distant directory path
	 * @throws Exception If module is deactivate
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return WebDAVClient
	 */
	protected function getWebDAVClient($directoryPath = '') {
		if (!$this->isActivate) {
			throw new Exception('Le module n\'est pas activé');
		} else {
			$config = array (
				'baseUri'   => $this->baseUri.$directoryPath,
				'userName'  => $this->login,
				'password'  => $this->password
            );

			return new \Sabre\DAV\Client($config);
		}
    }
}