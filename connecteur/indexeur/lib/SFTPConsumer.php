<?php

use phpseclib\Net\SFTP;
use phpseclib\Crypt\RSA;
use phpseclib\Net\SSH2;

/**
 * SFTPConsumer class to upload file on OpenData server with SFTP protocol.
 *
 * 
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class SFTPConsumer {

    /** Opendata server configuration */
    private $rootFolder;
    private $rootPath;
    private $openDataUrl;

    protected $sftp;

    const ROOT_PATH = '/var/www/vhosts/';

    public function __construct($isActivate, $host, $vhost, $rootFolder, $login, RSA $auth, $port = 22) {
        if (!empty($host)) {
            $this->rootFolder   = $rootFolder.(!empty($rootFolder) ? '/' : '');
            $this->openDataUrl  = 'http://'.$host.'/'.$this->rootFolder;

            // Shared hosting
            if (!empty($vhost)) {
                $this->rootPath = self::ROOT_PATH.$vhost.'/'.$this->rootFolder;
            } 
            // Hosting by domain name
            else {
                $hostArray = explode('.', $host);
                if (sizeof($hostArray) > 0) {
                    // If host is like data-mairie.girondenumerique.fr
                    if (strpos($hostArray[0], '-') !== false) {
                        $subDomainArray = explode('-', $hostArray[0]);
                        $this->rootPath = self::ROOT_PATH.$subDomainArray[0].'/'.$this->rootFolder;
                    } else {
                        $this->rootPath = self::ROOT_PATH.$hostArray[0].'/'.$this->rootFolder;
                    }
                } else {
                    throw new Exception('Impossible de résoudre le nom d\'hôte du serveur de fichiers');
                }
            }

            if ($isActivate) {
                $this->sftp = new SFTP($host, $port);
                if (!$this->sftp->login($login, $auth)) {
                    throw new Exception('Impossible de s\'authentifier sur le serveur de fichiers');
                }
            } else {
                throw new Exception('Le module n\'est pas activé');
            }
        } else {
            throw new Exception('Le serveur de fichiers n\'est pas renseigné');
        }
    }

    /**
	 * Test a connection to the opendata server.
	 *
     * @throws Exception If connection test fail
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return boolean
	 */
	public function testConnection() {
        $this->sftp->chdir($this->rootPath);
        if ($this->sftp->pwd()) {
            return true;
        } else {
            throw new Exception('Erreur : Impossible de se connecter au serveur OpenData');
        }
    }

    /**
     * Delete a file for an absolute file URL. Determines the directory from the file URL.
	 * 
     *
     * @param string $directory Distant directory into file must be deleted
	 * @param string $filename Document filename to delete
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return boolean
     */
    public function deleteDocumentFile(string $fileUrl) {
        $filename = basename($fileUrl);
        $directory = str_replace($filename, '', parse_url($fileUrl, PHP_URL_PATH));         // Delete file name from path
        $directory = ltrim($directory, '/');                                                // Delete first "/"
        $directory = str_replace($this->rootFolder, '', $directory);                        // Delete root folder to get only destination directory
        
        return $this->sftp->delete($this->rootPath.$directory.'/'.$filename);
    }

    /**
     * Upload document file into a specific directory on OpenData server. 
     * Use PUT stream cURL.
	 * 
     *
     * @param CitizenDocument $document Document and its informations
	 * @param DocumentFile $file File to upload
     * @param boolean $yearSubDirectory Flag to check/create sub-directory by year
     * @throws Exception If document does not exist
     * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
     * @return string
     */
    public function uploadDocumentFile(CitizenDocument $document, DocumentFile $file, bool $yearSubDirectory) {
        $fileUrl = NULL;
        $errmsg = NULL;
        $directory = '';
        if ($document->getDestinationDirectory() == ' aucun') {
            $directory = '';
        } else if (!empty($document->getDestinationDirectory())) {
            $directory = $document->getDestinationDirectory().'/';
        }

        // Check/create sub-directory if year has been fullfilled
        if ($yearSubDirectory) {
            $directory = $this->createSubDirectoryForYear($directory, $document->getDate());
        }

        if (file_exists($file->getPath())) {
            if ($this->sftp->put($this->rootPath.$directory.$file->getName(), $file->getPath(), SFTP::SOURCE_LOCAL_FILE)) {
                return $this->openDataUrl.$directory.$file->getName();
            } else {
                throw new Exception('Impossible de t&eacute;l&eacute;charger le fichier '.$file->getName().' dans le r&eacute;pertoire '.$directory.' du serveur OpenData');
            }
        } else {
            throw new Exception('Impossible d\'ouvrir le fichier '.$file->getPath());
        }
    }

    /**
	 * Search and create if not exists a sub-directory as year for a specific top directory.
     * If exists or created, this function return the relative path to the sub-directory.
	 *
     * @param string $topDirectory Top directory within must be a sub-directory as year
     * @param DateTime $documentDate Document date for year to search or create
     * @throws Exception If sub-directory does not exist and cannot be created
	 * @author Xavier MADIOT <x.madiot@girondenumerique.fr>
	 * @return string
	 */
    private function createSubDirectoryForYear(string $topDirectory, DateTime $documentDate) {
        $yearSubDirectory = $documentDate->format('Y');

        // Check if the subdirectory does not exist.
        if (!$this->sftp->is_dir($this->rootPath.$topDirectory.'/'.$yearSubDirectory)) {
            // Subdirectory creation
            if (!$this->sftp->mkdir($this->rootPath.$topDirectory.'/'.$yearSubDirectory, 0755, true)) {
                throw new Exception('Impossible de créer le sous-répertoire '.$yearSubDirectory.' dans le répertoire '.$topDirectory);
            }
        }

        // Whatever the year sub-directory is concat to the destination directory
        return $topDirectory.$yearSubDirectory.'/';        
    }
}