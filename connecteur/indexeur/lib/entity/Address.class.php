<?php

/**
 * Address entity class to describe an address.
 *
 * 
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class Address {

    private $street;

    private $complement;

    private $zipcode;

    private $city;

    private $cedexcode;

    private $postbox;

    public function __construct(){
        // Default constructor
    }

    /**
     * Get the value of street
     */ 
    public function getStreet() {
        return $this->street;
    }

    /**
     * Set the value of street
     *
     * @return  self
     */ 
    public function setStreet(string $street) {
        $this->street = $street;
        return $this;
    }

    /**
     * Get the value of complement
     */ 
    public function getComplement() {
        return $this->complement;
    }

    /**
     * Set the value of complement
     *
     * @return  self
     */ 
    public function setComplement(string $complement) {
        $this->complement = $complement;
        return $this;
    }

    /**
     * Get the value of zipcode
     */ 
    public function getZipcode() {
        return $this->zipcode;
    }

    /**
     * Set the value of zipcode
     *
     * @return  self
     */ 
    public function setZipcode(string $zipcode) {
        $this->zipcode = $zipcode;
        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity() {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity(string $city) {
        $this->city = $city;
        return $this;
    }

    /**
     * Get the value of cedexcode
     */ 
    public function getCedexcode() {
        return $this->cedexcode;
    }

    /**
     * Set the value of cedexcode
     *
     * @return  self
     */ 
    public function setCedexcode(string $cedexcode) {
        $this->cedexcode = $cedexcode;
        return $this;
    }

    /**
     * Get the value of postbox
     */ 
    public function getPostbox() {
        return $this->postbox;
    }

    /**
     * Set the value of postbox
     *
     * @return  self
     */ 
    public function setPostbox(string $postbox) {
        $this->postbox = $postbox;
        return $this;
    }
}