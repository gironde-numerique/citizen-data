<?php

include __DIR__.'/Address.class.php';

/**
 * Organization entity class to describe an establishment as organization.
 *
 * 
 * @version    1.1
 * @author 	   Xavier MADIOT <x.madiot@girondenumerique.fr>
 * @since      Class available since Pastell 2.0.12
 */ 
class Organization {

    private $siren;

    private $nic;

    private $name;

    /** @var  Address */
    private $address;

    private $category;

    private $legalCategory;

    private $activity;

    private $activityNomenclature;

    public function __construct(string $name, string $siren, string $nic) {
        $this->name     = $name;
        $this->siren    = $siren;
        $this->nic      = $nic;
    }

    /**
     * Get the value of siren
     */ 
    public function getSiren() {
        return $this->siren;
    }

    /**
     * Set the value of siren
     *
     * @return  self
     */ 
    public function setSiren(string $siren) {
        $this->siren = $siren;
        return $this;
    }

    /**
     * Get the value of nic
     */ 
    public function getNic() {
        return $this->nic;
    }

    /**
     * Set the value of nic
     *
     * @return  self
     */ 
    public function setNic(string $nic) {
        $this->nic = $nic;
        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName() {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName(string $name) {
        $this->name = $name;
        return $this;
    }

    /**
     * Get the value of address
     */ 
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set the value of address
     *
     * @return  self
     */ 
    public function setAddress(Address $address) {
        $this->address = $address;
        return $this;
    }

    /**
     * Get the value of category
     */ 
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set the value of category
     *
     * @return  self
     */ 
    public function setCategory(string $category) {
        $this->category = $category;
        return $this;
    }

    /**
     * Get the value of legalCategory
     */ 
    public function getLegalCategory() {
        return $this->legalCategory;
    }

    /**
     * Set the value of legalCategory
     *
     * @return  self
     */ 
    public function setLegalCategory(string $legalCategory) {
        $this->legalCategory = $legalCategory;
        return $this;
    }

    /**
     * Get the value of activity
     */ 
    public function getActivity() {
        return $this->activity;
    }

    /**
     * Set the value of activity
     *
     * @return  self
     */ 
    public function setActivity(string $activity) {
        $this->activity = $activity;
        return $this;
    }

    /**
     * Get the value of activityNomenclature
     */ 
    public function getActivityNomenclature() {
        return $this->activityNomenclature;
    }

    /**
     * Set the value of activityNomenclature
     *
     * @return  self
     */ 
    public function setActivityNomenclature(string $activityNomenclature) {
        $this->activityNomenclature = $activityNomenclature;
        return $this;
    }
}